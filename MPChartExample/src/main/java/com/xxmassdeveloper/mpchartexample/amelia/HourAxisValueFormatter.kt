package com.xxmassdeveloper.mpchartexample.amelia

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.ValueFormatter
import java.text.SimpleDateFormat
import java.util.*

class HourAxisValueFormatter() : ValueFormatter() {
    private val dataFormat = SimpleDateFormat("HH:mm", Locale.getDefault())

    override fun getFormattedValue(value: Float, axis: AxisBase): String {
        return getHour(value.toLong())
    }

    private fun getHour(timestamp: Long): String {
        return dataFormat.format(Date(timestamp))
    }
}

