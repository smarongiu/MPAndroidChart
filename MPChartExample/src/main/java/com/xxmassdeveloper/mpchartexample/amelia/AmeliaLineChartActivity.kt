package com.xxmassdeveloper.mpchartexample.amelia

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.SeekBar
import android.widget.TextView
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.xxmassdeveloper.mpchartexample.R
import com.xxmassdeveloper.mpchartexample.notimportant.DemoBase
import java.util.*
import kotlin.random.Random


class AmeliaLineChartActivity : DemoBase(), OnChartValueSelectedListener {

    private lateinit var chart: LineChart
    private lateinit var seekBarX: SeekBar
    private lateinit var seekBarY: SeekBar
    private lateinit var tvX: TextView
    private lateinit var tvY: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_linechart)

        title = this.javaClass.simpleName

        tvX = findViewById(R.id.tvXMax)
        tvY = findViewById(R.id.tvYMax)

        seekBarX = findViewById(R.id.seekBar1)
        seekBarY = findViewById(R.id.seekBar2)
        seekBarX.visibility = View.GONE
        seekBarY.visibility = View.GONE

        chart = findViewById(R.id.chart1)
        chart.setBackgroundColor(Color.TRANSPARENT)
        chart.setDrawBorders(false)
        chart.minOffset = 0f
        chart.setHardwareAccelerationEnabled(true)

        // no description text
        chart.getDescription().setEnabled(false)

        // touch gestures
        chart.setTouchEnabled(true)
        chart.setPinchZoom(false)
        chart.setDoubleTapToZoomEnabled(false)
        chart.setOnChartValueSelectedListener(this)

        // scaling and dragging
        chart.isDragEnabled = true
        chart.setScaleEnabled(false)
        chart.setDrawGridBackground(false)

        chart.isHighlightPerDragEnabled = false
        chart.isHighlightPerTapEnabled = true
        chart.setMaxHighlightDistance(100f)


        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        val mv = AmeliaMarkerView(this)
        chart.setMarker(mv)


        val xAxis = chart.xAxis.also { x ->
            x.isEnabled = true
            x.setTypeface(tfRegular)
            x.setLabelCount(10, false)
            x.textSize = 12f
            x.setDrawAxisLine(false)
            x.setTextColor(AmeliaColors.GRAY)
            x.setPosition(XAxis.XAxisPosition.BOTTOM)
            x.setDrawGridLines(false)

            x.setValueFormatter(HourAxisValueFormatter())
        }

        chart.axisLeft.isEnabled = false
        val yAxis = chart.axisRight.also { y ->
            y.isEnabled = true

            // horizontal grid lines
            //y.enableGridDashedLine(10f, 0f, 0f)
            y.gridLineWidth = 0.5f

            y.setTypeface(tfRegular)
            y.textSize = 14f
            //y.setLabelCount(6, false)
            y.setTextColor(AmeliaColors.GRAY)
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
            y.setDrawGridLines(true)
            y.setAxisLineColor(AmeliaColors.GRAY_200)

            y.setValueFormatter(TemperatureAxisValueFormatter())
        }

        LimitLine(23f, "Index A").also {
            it.lineWidth = 1f
            it.enableDashedLine(10f, 10f, 0f)
            it.labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
            it.textSize = 14f
            //yAxis.addLimitLine(it)
        }

        chart.setExtraOffsets(0f, 0f, 50f, 40f)

        // add data
        val refTime = Date().time - 24 * 3600 * 1000
        val xStep: Float = 3600f / 2 * 1000 //30m
        setData(randomValues(200, xStart = refTime.toFloat(), xStep = xStep, deltaMax = 10f, startValue = 20f))

        //chart.isAutoScaleMinMaxEnabled = true
        chart.setVisibleXRange(4f * 3600 * 1000, 8f * 3600 * 1000)

        chart.getLegend().setEnabled(false)
        chart.invalidate()
    }


    private fun newDataSet(label: String) = LineDataSet(emptyList(), label).apply {
        setMode(LineDataSet.Mode.CUBIC_BEZIER)
        setCubicIntensity(0.2f)
        setDrawFilled(true)
        setDrawCircles(false)
        setLineWidth(2f)
        setCircleRadius(4f)
        setCircleColor(AmeliaColors.BLUE_600)
        isHighlightEnabled = true
        setDrawHighlightIndicators(false)
        setColor(AmeliaColors.BLUE_600)
        setFillColor(AmeliaColors.BLUE_600)
        setFillAlpha(30)
        setDrawHorizontalHighlightIndicator(false)
        setFillFormatter(IFillFormatter { dataSet, dataProvider -> chart.axisLeft.axisMinimum })
    }


    private fun setData(values: List<Entry>) {
        val set1 = newDataSet("Set 1").apply { setValues(values) }

        // create a data object with the data sets
        val data = LineData(set1)
        data.setValueTypeface(tfLight)
        data.setValueTextSize(9f)
        data.setDrawValues(false)

        chart.setXAxisRenderer(ColoredLabelXAxisRenderer(chart.getViewPortHandler(), chart.getXAxis(), chart.getTransformer(YAxis.AxisDependency.LEFT), AmeliaColors.STONE_200_50P))

        // set data
        chart.data = data
    }

    override fun saveToGallery() {
    }

    private fun randomValues(count: Int = 100, xStart: Float = 0f, xStep: Float = 10f, deltaMax: Float = 5f, startValue: Float = 0f): List<Entry> {
        var x: Float = xStart
        var y: Float = startValue
        val values = (1..count).map {
            val delta = Random.nextFloat() * deltaMax - deltaMax / 2
            y += delta
            x += xStep
            Entry(x, y)
        }.toList()
        return values
    }

    override fun onValueSelected(e: Entry, h: Highlight) {
        chart.marker.refreshContent(e, h)
    }

    override fun onNothingSelected() {
    }
}


