package com.xxmassdeveloper.mpchartexample.amelia

import android.graphics.Color
import android.os.Bundle
import android.view.WindowManager
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.xxmassdeveloper.mpchartexample.R
import com.xxmassdeveloper.mpchartexample.notimportant.DemoBase
import java.util.*
import kotlin.random.Random

class AmeliaBarChartActivity : DemoBase(), OnChartValueSelectedListener {

    private lateinit var chart: BarChart

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_barchart_noseekbar)

        title = this.javaClass.simpleName

        chart = findViewById(R.id.chart1)
        chart.setBackgroundColor(Color.TRANSPARENT)
        chart.setDrawBorders(false)
        chart.minOffset = 0f
        chart.setHardwareAccelerationEnabled(false)

        chart.setRoundedCorners(5f)

        // no description text
        chart.getDescription().setEnabled(false)

        chart.setMaxVisibleValueCount(10)

        // touch gestures
        chart.setTouchEnabled(true)
        chart.setPinchZoom(false)
        chart.setDoubleTapToZoomEnabled(false)
        chart.setOnChartValueSelectedListener(this)

        // scaling and dragging
        chart.isDragEnabled = true
        chart.setScaleEnabled(false)
        chart.setDrawGridBackground(false)

        chart.isHighlightPerDragEnabled = false
        chart.isHighlightPerTapEnabled = true
        chart.setMaxHighlightDistance(100f)


        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        val mv = AmeliaMarkerView(this, false)
        chart.setMarker(mv)


        val xAxis = chart.xAxis.also { x ->
            x.isEnabled = true
            x.setTypeface(tfRegular)
            x.setLabelCount(10, false)
            x.textSize = 12f
            x.setDrawAxisLine(false)
            x.setTextColor(AmeliaColors.GRAY)
            x.setPosition(XAxis.XAxisPosition.BOTTOM)
            x.setDrawGridLines(false)
        }

        chart.axisLeft.isEnabled = false
        val yAxis = chart.axisRight.also { y ->
            y.isEnabled = true

            // horizontal grid lines
            y.gridLineWidth = 0.5f

            y.setTypeface(tfRegular)
            y.textSize = 14f
            y.setTextColor(AmeliaColors.GRAY)
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
            y.setDrawGridLines(true)
            y.setDrawAxisLine(false)

            y.setAxisLineColor(AmeliaColors.GRAY_200)

            y.setValueFormatter(MinuteAxisValueFormatter())
        }

        LimitLine(28f, "Goal").also {
            it.lineWidth = 0.5f
            it.enableDashedLine(10f, 10f, 0f)
            it.labelPosition = LimitLine.LimitLabelPosition.LEFT_TOP
            it.textSize = 12f
            it.textColor = Color.RED
            yAxis.addLimitLine(it)
        }

        chart.setExtraOffsets(0f, 0f, 50f, 40f)

        ///// set data /////
        val refDateMillis= Date().time
        val xUnitInMillis = 24 * 3600 * 1000
        setData(randomValues(20, xStart = 0f, xStep = 1f, range = (0..60), startValue = 10f))

        xAxis.setValueFormatter(DateAxisValueFormatter(refDateMillis, xUnitInMillis))

        chart.setVisibleXRangeMaximum(8f)

        chart.getLegend().setEnabled(false)
        chart.invalidate()
    }


    private fun newDataSet(label: String, values: List<BarEntry>) = BarDataSet(values, label).apply {
        isHighlightEnabled = true
        color = AmeliaColors.LIGHT_ORANGE
        highLightColor = AmeliaColors.ORANGE
        highLightAlpha = 240
        setDrawIcons(false)
    }


    private fun setData(values: List<BarEntry>) {
        val set1 = newDataSet("Set 1", values)

        // create a data object with the data sets
        val data = BarData(set1)
        data.setDrawValues(false)
        data.barWidth = 0.56f

        chart.setXAxisRenderer(ColoredLabelXAxisRenderer(chart.getViewPortHandler(), chart.getXAxis(), chart.getTransformer(YAxis.AxisDependency.LEFT), AmeliaColors.STONE_200_50P))
        chart.setData(data)
        chart.setFitBars(true)
        chart.invalidate()
    }

    override fun saveToGallery() {
    }

    private fun randomValues(count: Int = 100, xStart: Float = 0f, xStep: Float = 10f, range: IntRange = (0..60), startValue: Float = 0f): List<BarEntry> {
        var x: Float = xStart
        var y: Float = startValue
        val values = (1..count).map {
            y = Random.nextFloat() * (range.last - range.first) + range.first
            x += xStep
            BarEntry(x, y)
        }.toList()
        return values
    }

    override fun onValueSelected(e: Entry, h: Highlight) {
        chart.marker.refreshContent(e, h)
    }

    override fun onNothingSelected() {
    }

}