package com.xxmassdeveloper.mpchartexample.notimportant

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.github.mikephil.charting.utils.Utils
import com.xxmassdeveloper.mpchartexample.R
import com.xxmassdeveloper.mpchartexample.amelia.AmeliaBarChartActivity
import com.xxmassdeveloper.mpchartexample.amelia.AmeliaLineChartActivity
import java.util.*

class AmeliaMainActivity : AppCompatActivity(), OnItemClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_main)

        title = "MPAndroidChart POC"

        // initialize the utilities
        Utils.init(this)

        val objects = ArrayList<ContentItem>()

        ////
        objects.add(0, ContentItem("Amelia 1", "Amelia sample line chart."))
        objects.add(1, ContentItem("Amelia 2", "Amelia sample bar chart."))

        val adapter = MyAdapter(this, objects)

        val lv: ListView = findViewById(R.id.listView1)
        lv.setAdapter(adapter)

        lv.setOnItemClickListener(this)
    }

    override fun onItemClick(av: AdapterView<*>, v: View, pos: Int, arg3: Long) {

        var i: Intent? = null

        when (pos) {
            0 -> i = Intent(this, AmeliaLineChartActivity::class.java)
            1 -> i = Intent(this, AmeliaBarChartActivity::class.java)
        }

        if (i != null) startActivity(i)

        overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

}
