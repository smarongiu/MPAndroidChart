package com.xxmassdeveloper.mpchartexample.amelia

import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi

class AmeliaColors {
    companion object {
        val GRAY_200 = Color.parseColor("#bec6da")
        val BLUE_600 = Color.parseColor("#00b0e4")
        val GRAY = Color.GRAY
        val ORANGE = Color.parseColor("#ff7744")
        val LIGHT_ORANGE = Color.parseColor("#4cff7744") //30% opacity

        @RequiresApi(Build.VERSION_CODES.O)
        val STONE_200_50P = Color.argb(128,241, 244, 250)
    }
}