package com.xxmassdeveloper.mpchartexample.amelia

import com.github.mikephil.charting.formatter.ValueFormatter
import java.text.SimpleDateFormat
import java.util.*

class DateAxisValueFormatter(val referenceDateTimeMillis: Long, val xUnitInMillis: Int) : ValueFormatter() {
    private val dataFormat = SimpleDateFormat("dd/MM/YY", Locale.getDefault())

    override fun getFormattedValue(value: Float): String {
        val offset = value.toLong()
        return dataFormat.format(Date(referenceDateTimeMillis + offset * xUnitInMillis))
    }
}