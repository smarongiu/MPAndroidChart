package com.xxmassdeveloper.mpchartexample.amelia

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.renderer.XAxisRenderer
import com.github.mikephil.charting.utils.MPPointF
import com.github.mikephil.charting.utils.Transformer
import com.github.mikephil.charting.utils.Utils
import com.github.mikephil.charting.utils.ViewPortHandler

class ColoredLabelXAxisRenderer : XAxisRenderer {

    internal var labelBackgroundColor: Int

    constructor(viewPortHandler: ViewPortHandler, xAxis: XAxis, trans: Transformer, bgColor: Int = Color.TRANSPARENT) : super(viewPortHandler, xAxis, trans) {
        this.labelBackgroundColor = bgColor
    }

    protected override fun drawLabels(c: Canvas, pos: Float, anchor: MPPointF) {
        val labelRotationAngleDegrees = mXAxis.labelRotationAngle
        val centeringEnabled = mXAxis.isCenterAxisLabelsEnabled
        val axisHeight = mViewPortHandler.offsetBottom()
        val axisWidth = mViewPortHandler.chartWidth

        val positions = FloatArray(mXAxis.mEntryCount * 2)

        //draw background
        val bgPaint = Paint().apply {
            color = labelBackgroundColor
            strokeWidth = 20f
        }
        c.drawRect(RectF(0f, pos - mAxis.yOffset, axisWidth, pos + axisHeight), bgPaint)


        run {
            var i = 0
            while (i < positions.size) {

                // only fill x values
                if (centeringEnabled) {
                    positions[i] = mXAxis.mCenteredEntries[i / 2]
                } else {
                    positions[i] = mXAxis.mEntries[i / 2]
                }
                i += 2
            }
        }

        mTrans.pointValuesToPixel(positions)

        var i = 0
        while (i < positions.size) {
            var x = positions[i]
            if (mViewPortHandler.isInBoundsX(x)) {
                val label = mXAxis.valueFormatter.getFormattedValue(mXAxis.mEntries[i / 2], mXAxis)
                mAxisLabelPaint.color = mXAxis.textColor
                if (mXAxis.isAvoidFirstLastClippingEnabled) {
                    // avoid clipping of the last
                    if (i == mXAxis.mEntryCount - 1 && mXAxis.mEntryCount > 1) {
                        val width = Utils.calcTextWidth(mAxisLabelPaint, label)
                        if (width > mViewPortHandler.offsetRight() * 2 && x + width > mViewPortHandler.chartWidth)
                            x -= width / 2
                    } else if (i == 0) { // avoid clipping of the first
                        val width = Utils.calcTextWidth(mAxisLabelPaint, label)
                        x += width / 2
                    }
                }
                drawLabel(c, label, x, pos + axisHeight / 2 - mXAxis.mLabelHeight, anchor, labelRotationAngleDegrees)
            }
            i += 2
        }
    }

}