package com.xxmassdeveloper.mpchartexample.amelia

import com.github.mikephil.charting.formatter.ValueFormatter

class MinuteAxisValueFormatter() : ValueFormatter() {
    override fun getFormattedValue(value: Float): String {
        return "${value.toInt()} min"
    }
}