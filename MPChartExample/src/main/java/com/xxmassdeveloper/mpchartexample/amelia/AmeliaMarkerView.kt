package com.xxmassdeveloper.mpchartexample.amelia

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.Log
import android.widget.TextView
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import com.xxmassdeveloper.mpchartexample.R
import java.text.SimpleDateFormat
import java.util.*

class AmeliaMarkerView(context: Context, val drawCircle: Boolean = true) : MarkerView(context, R.layout.amelia_marker_view) {
    private val dataFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
    val tvContent: TextView = findViewById(R.id.tvContent)

    val yBubble = 200f
    val linePaint = Paint().apply {
        color = AmeliaColors.GRAY_200
        strokeWidth = 2f
    }
    val circlePaint = Paint().apply {
        color = AmeliaColors.BLUE_600
        strokeWidth = 4f
        isAntiAlias = true
        style = Paint.Style.STROKE
    }
    val circleFillPaint = Paint().apply {
        color = Color.WHITE
        isAntiAlias = true
        style = Paint.Style.FILL
    }
    val circleRadius = 8f

    override fun refreshContent(e: Entry, highlight: Highlight) {
        Log.d("", "AmeliaMarkerView::refreshContent")

        val value = Math.round(e.y)
        val t = dataFormat.format(Date(e.x.toLong())) //assuming entry is a time-based value

        tvContent.text = "${value}° at ${t}"
        super.refreshContent(e, highlight)
    }

    override fun draw(canvas: Canvas?, posX: Float, posY: Float) {
        canvas?.also { c ->
            c.drawLine(posX, posY, posX, yBubble + height, linePaint)
            if (drawCircle) {
                c.drawCircle(posX, posY, circleRadius, circleFillPaint)
                c.drawCircle(posX, posY, circleRadius, circlePaint)
            }
            super.draw(c, posX, yBubble)
        }
    }

    override fun getOffset(): MPPointF {
        return MPPointF((-width / 2).toFloat(), 0f)
    }
}